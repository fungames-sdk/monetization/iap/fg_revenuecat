# Revenuecat

## Integration Steps

1) **"Install"** or **"Upload"** FGRevenuecat plugin from the FunGames Integration Manager.

2) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.

3) Setup your IAP products in FGIAPSettings (see [FG IAP](https://sdk-doc-static-website-dot-carbide-booth-229112.ew.r.appspot.com/fg_iap.html#iap))

4) Add your API Keys in **FGRevenuecatSettings** and make sure that Purchases component in FGRevenuecat prefab is set as follow :

![](_source/fg_revenuecat_purchases.png)

## Adjust Integration

In FG, we use Observer Mode so that Revenuecat automatically track every purchases. These purchases are then forwarded to Adjust through S2S, using "IAP_Total" event token. This is handled on RC dashboard side, no action is required by the developer for it.

But to ensure proper integration, please make sure that :

- No "IAP_Total" event is sent through Adjust SDK in your app. (This is to avoid duplicate revenue tracking from "IAP_Total")
- No revenues are sent with other AdjustEvents in your app. (This is to avoid duplicate revenue tracking from other AdjustEvents, since all AdjustEvents with revenues are considered as "IAP" events by Adjust)

So if you already integrate AdjustEvent in your app, please make sure that no revenues are attached to it anymore, and that the token for "IAP_Total" is not used in game code.